﻿Public Class TA_Action
    Inherits TA_BasicGameObject
    Public Sub New(ByVal Id As String, ByVal Title As String)
        MyBase.New

        Me.ID = Id
        Me.Title = Title
    End Sub

    Public Property CommandKeyWord As String
    Public Property ObjectKeyWord As String
    Public Property MovementTarget As String

End Class
