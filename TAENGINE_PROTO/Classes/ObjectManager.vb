﻿Public Class ObjectManager

    Public Locations As New List(Of TA_Location)

    Public Actions As New List(Of TA_Action)

    Public Header As New TA_BasicGameObject

    Private Logoutput As Control




    Public Sub New(ByRef Adventure As TADB_3Musketeers, ByRef SomeControl As Control)

        Adventure.LocationList = Me.Locations
        Adventure.ActionList = Me.Actions
        Adventure.Header = Me.Header

        Adventure.InitializeAdventure()
        Logoutput = SomeControl


        'Test
        For Each item In Locations
            LogEntry(item.ID + ": " + item.Title + " added.")
        Next
        For Each item In Actions
            LogEntry(item.ID + ": " + item.Title + " added.")
        Next
    End Sub



    Private Sub LogEntry(ByVal LogItem As String)

        Logoutput.Text = LogItem + vbCrLf + Logoutput.Text


    End Sub

End Class
