﻿Public Class TA_BasicGameObject
    Public Sub New()
        Me.Actions = ""
        Me.Inventory = ""
        Me.Title = ""
        Me.ID = ""
        Me.Comment = ""
        Me.VerboseDescription = ""
        Me.ShortDescription = ""


    End Sub
    Public Enum TA_ObjectType
        Location
        Player
        Item
        Event_Command_Movement
        Event_Command
    End Enum


    ''' <summary>
    ''' Eindeutige Id zum Aufrufen des Objekts
    ''' </summary>
    ''' <returns>String, Konvention noch offen, eindeutig mindestens für den Objekttyp</returns>
    Public Property ID As String

    '
    ''' <summary>
    ''' Bezeichnender Titel des Objekts -> Intern und für spätere Nutzung eines Editors
    ''' </summary>
    ''' <returns>String</returns>
    Public Property Title As String

    ''' <summary>
    ''' Kommentar des Objekts -> Intern und für spätere Nutzung eines Editors
    ''' </summary>
    ''' <returns>String</returns>
    Public Property Comment As String


    ''' <summary>
    ''' Ausführliche Beschreibung die beim ersten Kontakt des Spielers mit dem Objekt angezeigt werden soll. Danach nur bei Befehl zur Inspektion
    ''' </summary>
    ''' <returns>Für den Anfang reicht ein einzelner String</returns>
    ''' <remarks>  Hinter diese Property könnte später eine Liste mit verschiedenen alternativen Beschreibungen implementiert werden, die z.B. auf Tageszeit o.Ä. reagiert.</remarks>
    Public Property VerboseDescription As String


    ''' <summary>
    ''' Kurze Beschreibung für das Objekt.
    ''' </summary>
    ''' <returns>Hinter diese Property könnte später eine Liste mit verschiedenen alternativen Beschreibungen implementiert werden, die z.B. auf Tageszeit o.Ä. reagiert.</returns>
    ''' <remarks>Die ausführliche Beschreibung kann über VerboseDescription abgerufen werden.
    ''' </remarks>
    Public Property ShortDescription As String


    Private _FirstTime As Boolean = True
    ''' <summary>
    ''' Erste Verwendung
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>  kann verwendet werden, um ersten Zugriff zu dokumentieren, z.B. für VerboseDescription</remarks>
    Public Property FirstTime As Boolean
        Get
            Return _FirstTime
        End Get
        Set(value As Boolean)
            _FirstTime = value
        End Set
    End Property

    ''' <summary>
    ''' Ausführlicher Objekttyp, Enum (wird in der Basisklasse bereitgestellt
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Kann verwendet werden um die Funktionalität genauer zu definieren, z.B. Bewegungsbefehl oder Zufallsereignis etc.</remarks>
    Public Property ObjectType As TA_ObjectType

    Private _MyStatus As Boolean = False
    ''' <summary>
    ''' Generischer Status, Boolean
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Z.B. um festzustellen, ob ein Ereignis aktiv ist, eine Tür offen usw.</remarks>
    Public Property MyStatus As Boolean
        Get
            Return _MyStatus
        End Get
        Set(value As Boolean)
            _MyStatus = value
        End Set
    End Property

    Private _MyCount As Integer = 0

    ''' <summary>
    ''' Generischer Zähler für beliebige Verwendung
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Könnte z.B. für Geld, Anzahl Züge etc. verwendet werden.</remarks>
    Public Property MyCount As Integer
        Get
            Return _MyCount
        End Get
        Set(value As Integer)
            _MyCount = value
        End Set
    End Property

#Region "Inventar"


    ''' <summary>
    ''' Ein String mit den ID's aller Objekte die sich in diesem Objekt befinden, durch Komma getrennt
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Erleichtert das Speichern. Für das Zufügen, Entfernen sowie prüfen, ob ein bestimmtes Objekt im Inventar sind, gibt es zusätzliche Methoden</remarks>
    Public Property Inventory As String

    ''' <summary>
    ''' Prüft ob ObjectId im Inventar ist 
    ''' </summary>
    ''' <param name="ObjectId"></param>
    ''' <returns></returns>
    Public Function IsItemInInventory(ByVal ObjectId As String) As Boolean

        Return IsIdInString(ObjectId, Inventory)

    End Function

    ''' <summary>
    ''' Fügt ein Objekt in das Inventar ein, soweit es noch nicht vorhanden ist
    ''' </summary>
    ''' <param name="ObjectId"></param>
    Public Sub AddItemToInventory(ByVal ObjectId As String)

        Inventory = AddIdToString(ObjectId, Inventory)

    End Sub

    ''' <summary>
    ''' Entfernt ein Objekt aus dem Inventar, soweit es vorhanden ist
    ''' </summary>
    ''' <param name="ObjectId"></param>
    Public Sub RemoveItemFromInventory(ByVal ObjectId As String)

        Inventory = RemoveIdFromString(ObjectId, Inventory)

    End Sub

#End Region


#Region "Aktionen"
    ''' <summary>
    ''' Eine Liste von Ereignis/Aktions ID's durch Komma getrennt, ähnlich wie bei Inventar
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Es werden ähnliche Funktionen bereitgestellt wie beim Inventar, ggf. werden auch später andere benötigt</remarks>
    Public Property Actions As String
    ' Actions="M1,M2,M3"
    ''' <summary>
    ''' Prüft ob eine Aktion zum Objekt gehört
    ''' </summary>
    ''' <param name="ObjectID"></param>
    ''' <returns></returns>
    Public Function IsMyAction(ByVal ObjectID As String) As Boolean

        Return IsIdInString(ObjectID, Actions)

    End Function

    ''' <summary>
    ''' Eine Aktion hinzufügen
    ''' </summary>
    ''' <remarks>Generisch, eventuell wenig benötigt</remarks>
    Public Sub AddAction(ByVal ObjectId)

        Actions = AddIdToString(ObjectId, Actions)

    End Sub


    Public Sub RemoveAction(ByVal ObjectId)

        Actions = RemoveIdFromString(ObjectId, Actions)


    End Sub

#End Region


#Region "Logik"

    'Sucht eine Id in einer Anzahl durch Kommata getrennten IDs in einem String
    Private Function IsIdInString(ByVal Id As String, IdList As String) As Boolean

        Dim ItemFound As Boolean = False

        Dim ItemList As String() = IdList.Split(",", StringSplitOptions.TrimEntries)
        For Each Item In ItemList
            If Item = Id Then
                ItemFound = True
                Exit For

            End If
        Next
        Return ItemFound

    End Function

    Private Function AddIdToString(ByVal Id As String, IdList As String) As String

        'Wenn der Gegenstand schon im Inventar ist, überspringen (Doppel müssen über den Counter gelöst werden)
        If IsIdInString(Id, IdList) Then
            Return IdList
        Else
            'Ansonsten einfach hinten anhängen. Wenn das Inventar nicht leer ist, ein Komma setzen

            If IdList.Trim() <> "" Then
                IdList += ","
            End If

            Return IdList + Id.Trim()

        End If

    End Function

    Private Function RemoveIdFromString(ByVal Id As String, IdList As String) As String

        Dim RemoveString As String = "," + IdList
        '
        'ID und führendes Komma entfernen
        RemoveString = RemoveString.Replace("," + Id, "")

        'Führendes Komma im Hauptstring wieder entfernen
        If RemoveString.Length > 0 AndAlso RemoveString.Substring(0, 1) = "," Then
            RemoveString = RemoveString.Substring(1) 'Erstes Element übergehen
        End If

        Return RemoveString

    End Function


#End Region
End Class
