﻿Public Class StartForm
    Private AdventureManager As ObjectManager

    Public Sub New()


        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.


        Dim Adventure As New TADB_3Musketeers
        AdventureManager = New ObjectManager(Adventure, Me.rtbLog)

        Me.Text = AdventureManager.Header.ShortDescription
        Me.lblTitel.Text = AdventureManager.Header.ShortDescription
        Me.lblTitle2.Text = AdventureManager.Header.VerboseDescription


    End Sub
End Class
