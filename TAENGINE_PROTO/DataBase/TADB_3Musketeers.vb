﻿Public Class TADB_3Musketeers
    Inherits TADB_Base
    'Initialisiert das ABenteuer. Soll später über eine Datenbank oder XML / JSON Datei gefüllt werden.

    Public Shadows Sub InitializeAdventure()

        AddLocations()

        Me.Header.ID = "ADV1"
        Me.Header.Title = "Die drei Musketiere"
        Me.Header.ShortDescription = "Die drei Musketiere"
        Me.Header.VerboseDescription = "Ein Textadventure nach den Romanen und Charakteren von A. Dumas"


    End Sub


    Private Sub AddLocations()

        'Gut der d'Artagnans
        'Hof

        Dim Location As TA_Location
        Dim Action As TA_Action
        Location = New TA_Location("L1", "Dartagnan_Hof")
        Location.ShortDescription = "Hof des Gutes der d'Artagnans"
        Location.VerboseDescription = "Du befindest dich auf dem ungepflasterten Innenhof des Gutes der d'Artagnans. Einige Hühner picken im Stroh nach Körnern. Du siehst das Gutshaus, einen Stall sowie das Tor zur Straße."
        Location.AddAction("L1M1")

        Action = New TA_Action("L1M1", "HofStall")
        Action.ShortDescription = "in den Stall"
        Action.ObjectType = TA_BasicGameObject.TA_ObjectType.Event_Command_Movement
        Action.CommandKeyWord = "Gehe"
        Action.ObjectKeyWord = "Stall"
        Action.MovementTarget = "L2"
        Me.ActionList.Add(Action)

        'Stall: Hier Pferd holen


        Me.LocationList.Add(Location)

        Location = New TA_Location("L2", "Ställe")
        Location.ShortDescription = "Stall auf Gut d'Artagnan"
        Location.VerboseDescription = "Schweine dösen in den Pferchen, während fette Schmeißfliegen träge herumsummen. Am Ende des Ganges stehen einige Ackergäule. Zum Hof ist der Stall offen."
        Location.AddAction("L2M1")

        Me.LocationList.Add(Location)

        Action = New TA_Action("L2M1", "StallHof")
        Action.ShortDescription = "in den Hof"
        Action.ObjectType = TA_BasicGameObject.TA_ObjectType.Event_Command_Movement
        Action.CommandKeyWord = "Gehe"
        Action.ObjectKeyWord = "Hof, raus"
        Action.MovementTarget = "L1"
        Me.ActionList.Add(Action)








    End Sub


End Class
