﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class StartForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(StartForm))
        Me.rtbLog = New System.Windows.Forms.RichTextBox()
        Me.lblLog = New System.Windows.Forms.Label()
        Me.tbCommand = New System.Windows.Forms.TextBox()
        Me.lblCommand = New System.Windows.Forms.Label()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.lblTitel = New System.Windows.Forms.Label()
        Me.lblTitle2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'rtbLog
        '
        Me.rtbLog.Location = New System.Drawing.Point(39, 793)
        Me.rtbLog.Name = "rtbLog"
        Me.rtbLog.ReadOnly = True
        Me.rtbLog.Size = New System.Drawing.Size(1665, 115)
        Me.rtbLog.TabIndex = 0
        Me.rtbLog.Text = ""
        '
        'lblLog
        '
        Me.lblLog.AutoSize = True
        Me.lblLog.Location = New System.Drawing.Point(45, 755)
        Me.lblLog.Name = "lblLog"
        Me.lblLog.Size = New System.Drawing.Size(89, 15)
        Me.lblLog.TabIndex = 1
        Me.lblLog.Text = "Systemausgabe"
        '
        'tbCommand
        '
        Me.tbCommand.Location = New System.Drawing.Point(40, 671)
        Me.tbCommand.Name = "tbCommand"
        Me.tbCommand.Size = New System.Drawing.Size(1664, 23)
        Me.tbCommand.TabIndex = 2
        '
        'lblCommand
        '
        Me.lblCommand.AutoSize = True
        Me.lblCommand.Location = New System.Drawing.Point(40, 624)
        Me.lblCommand.Name = "lblCommand"
        Me.lblCommand.Size = New System.Drawing.Size(46, 15)
        Me.lblCommand.TabIndex = 3
        Me.lblCommand.Text = "Befehle"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Location = New System.Drawing.Point(39, 175)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.ReadOnly = True
        Me.RichTextBox1.Size = New System.Drawing.Size(1665, 417)
        Me.RichTextBox1.TabIndex = 4
        Me.RichTextBox1.Text = ""
        '
        'lblTitel
        '
        Me.lblTitel.AutoSize = True
        Me.lblTitel.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point)
        Me.lblTitel.Location = New System.Drawing.Point(40, 52)
        Me.lblTitel.Name = "lblTitel"
        Me.lblTitel.Size = New System.Drawing.Size(64, 32)
        Me.lblTitel.TabIndex = 5
        Me.lblTitel.Text = "Titel"
        '
        'lblTitle2
        '
        Me.lblTitle2.AutoSize = True
        Me.lblTitle2.Location = New System.Drawing.Point(50, 100)
        Me.lblTitle2.Name = "lblTitle2"
        Me.lblTitle2.Size = New System.Drawing.Size(56, 15)
        Me.lblTitle2.TabIndex = 6
        Me.lblTitle2.Text = "Untertitel"
        '
        'StartForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(1757, 935)
        Me.Controls.Add(Me.lblTitle2)
        Me.Controls.Add(Me.lblTitel)
        Me.Controls.Add(Me.RichTextBox1)
        Me.Controls.Add(Me.lblCommand)
        Me.Controls.Add(Me.tbCommand)
        Me.Controls.Add(Me.lblLog)
        Me.Controls.Add(Me.rtbLog)
        Me.Name = "StartForm"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents rtbLog As RichTextBox
    Friend WithEvents lblLog As Label
    Friend WithEvents tbCommand As TextBox
    Friend WithEvents lblCommand As Label
    Friend WithEvents RichTextBox1 As RichTextBox
    Friend WithEvents lblTitel As Label
    Friend WithEvents lblTitle2 As Label
End Class
